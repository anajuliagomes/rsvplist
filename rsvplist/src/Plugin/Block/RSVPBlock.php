<?php

namespace Drupal\rsvplist\Plugin\Block;

/**
 * @file
 * Contains \Drupal\rsvplist\Plugin\Block\RSVPBlock.
 */

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides an 'RSVP' List Block.
 *
 * @Block(
 *  id = "rsvp_block",
 *  admin_label = @Translation("RSVP Block"),
 * )
 */
class RSVPBlock extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\rsvplist\Form\RSVPForm');
  }

  /**
   * Displays RSVP Block if access is granted.
   *
   * @param Drupal\Core\Session\AccountInterface $account
   *   Checks user account.
   */
  public function blockAccess(AccountInterface $account) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = \Drupal::routeMatch()->getParameter('node');
    $nid = NULL;
    /**
     * @var \Drupal\rsvplist\EnablerService $enabler
     */
    $enabler = \Drupal::service('rsvplist.enabler');
    if ($node) {
      if ($enabler->isEnabled($node)) {
        return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
      }
    }
    return AccessResult::forbidden();
  }

}
